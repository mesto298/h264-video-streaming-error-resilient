import numpy as np

def polar_transform(N, channel_indices):
    if N == 1:
        return channel_indices

    even_indices = channel_indices[:N // 2]
    odd_indices = channel_indices[N // 2:]

    even_transformed = polar_transform(N // 2, even_indices)
    odd_transformed = polar_transform(N // 2, odd_indices)

    return np.concatenate([even_transformed, odd_transformed])

def polar_encode(message):
    N = len(message)
    channel_indices = np.arange(N)
    transformed_indices = polar_transform(N, channel_indices)

    encoded_message = np.zeros(N, dtype=int)
    encoded_message[transformed_indices] = message

    return encoded_message

def polar_decode(received_codeword):
    N = len(received_codeword)
    channel_indices = np.arange(N)
    transformed_indices = polar_transform(N, channel_indices)

    sorted_indices = np.argsort(transformed_indices)
    received_codeword = received_codeword[sorted_indices]

    decoded_message = np.zeros(N, dtype=int)

    for i in range(N // 2):
        u1 = decoded_message[:N // 2]
        u2 = received_codeword[N // 2:]

        decoded_message[:N // 2] = u1 ^ u2

    return decoded_message
