import socket
from bitstring import BitArray
import subprocess
from polar_code import polar_decode

def bitstream_to_h264(bitstream):
    # Convert binary bitstream to H.264 data
    return BitArray(bin=bitstream).bytes

def receive_video(server_address, enable_polar_decoding=True):
    client_socket = None  # Declare client_socket outside the try block
    process = None

    try:
        # Video decoding and displaying
        command = "ffplay -"
        process = subprocess.Popen(command.split(), stdin=subprocess.PIPE, stderr=subprocess.PIPE)

        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(server_address)

        while True:
            # Receive the binary bitstream
            bitstream = client_socket.recv(4096).decode()  # Adjust the buffer size if needed
            if not bitstream:
                break

            # Apply polar decoding if enabled
            if enable_polar_decoding:
                polar_decoded_bitstream = polar_decode(bitstream)
                print(f"Received Polar Decoded Bitstream: {polar_decoded_bitstream}")
                h264_data = bitstream_to_h264(polar_decoded_bitstream)
            else:
                print(f"Received Bitstream: {bitstream}")
                h264_data = bitstream_to_h264(bitstream)

            # Write the raw byte data to the process
            process.stdin.write(h264_data)

        # Check for errors in the ffplay process
        stderr_output = process.stderr.read()
        if stderr_output:
            raise Exception(f"ffplay error: {stderr_output.decode()}")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        if client_socket:
            # Close the socket and wait for the process to finish
            client_socket.close()
        if process:
            process.stdin.close()
            process.stderr.close()
            process.wait()

def main():
    server_address = ("127.0.0.1", 12345)
    # Pass True or False to enable or disable polar decoding, respectively
    receive_video(server_address, enable_polar_decoding=True)

if __name__ == "__main__":
    main()
