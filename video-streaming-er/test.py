import matplotlib.pyplot as plt
import numpy as np
import math

# Initial conditions of the wave
mean = 0
std = 0.1
timestep = 0.002 # 500 Hz
endtime = 45 #sec
numsam = int(endtime/timestep)
t = np.linspace(0, endtime, numsam)
samples = np.random.normal(mean, std, size=numsam)
data = np.zeros((numsam,2))
## Decide Amplification factor
data[:, 0] = [i*750 for i in samples]

# Make the both ends tapered
taperedtime = 5 #sec
x=np.linspace(-4, 0, int(taperedtime/timestep))
taperfunc = []
for x in x:
    taperfunc.append(math.exp(x))

for i in range(numsam-5000):
    taperfunc.append(1)

taperend = np.linspace(0,4,2500)
for x in taperend:
    taperfunc.append(1/math.exp(x))

print(len(taperfunc))
for i in range(numsam):
    data[i, 1] = data[i, 0]*taperfunc[i]

# Save the generated wave as a text file and shows a graph of it.
save = np.savetxt("white_noise_01.txt", data[:, 1], delimiter=',')
fig, ax = plt.subplots(figsize=(20,5))
xf = np.linspace(0.0,numsam*0.002, numsam)
ax.plot(xf,data[:,1], color='k', linewidth=0.5)
ax.grid(True)
plt.show()