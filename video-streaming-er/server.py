# server.py
import socket
import subprocess
from polar_code import polar_encode
from bitstring import BitArray
import random

def h264_to_bitstream(h264_data):
    return BitArray(bytes=h264_data).bin

def add_noise(bitstream, enable_noise=True):
    if enable_noise:
        noise_ratio = 0.01  # Adjust the noise ratio as needed
        noise = ''.join(random.choice(['0', '1']) for _ in range(len(bitstream)))
        bitstream = ''.join(
            b if random.random() > noise_ratio else n for b, n in zip(bitstream, noise)
        )
    return bitstream

def send_video(client_socket, enable_noise=False, enable_polar_coding=True):
    try:
        command = "ffmpeg -i videos/test.mp4 -c:v libx264 -f h264 -"
        process = subprocess.Popen(command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        while True:
            data = process.stdout.read(4096)
            if not data:
                break

            bitstream = h264_to_bitstream(data)

            # Add noise to the bitstream if enabled
            bitstream_with_noise = add_noise(bitstream, enable_noise)

            # Apply polar encoding if enabled
            if enable_polar_coding:
                polar_encoded_bitstream = polar_encode(bitstream_with_noise)
                client_socket.sendall(polar_encoded_bitstream.encode())
            else:
                client_socket.sendall(bitstream_with_noise.encode())

        stderr_output = process.stderr.read()
        if stderr_output:
            raise Exception(f"ffmpeg error: {stderr_output.decode()}")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        client_socket.close()
        process.stdout.close()
        process.stderr.close()
        process.wait()

def main():
    try:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind(("127.0.0.1", 12345))
        server_socket.listen(1)

        print("Server listening on port 12345...")

        client_socket, client_address = server_socket.accept()
        print(f"Connection from {client_address}")

        # Pass True or False to enable or disable noise, respectively
        # Pass True or False to enable or disable polar coding, respectively
        send_video(client_socket, enable_noise=False, enable_polar_coding=True)

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        server_socket.close()

if __name__ == "__main__":
    main()
