def find_checksum(sent_message, k):
    # Dividing sent message into packets of k bits.
    c1 = sent_message[:k]
    c2 = sent_message[k:2*k]
    c3 = sent_message[2*k:3*k]
    c4 = sent_message[3*k:4*k]

    # Calculating the binary sum of packets
    checksum_sum = bin(int(c1, 2) + int(c2, 2) + int(c3, 2) + int(c4, 2))[2:]

    # Adding the overflow bits
    if len(checksum_sum) > k:
        x = len(checksum_sum) - k
        checksum_sum = bin(int(checksum_sum, 2) >> x)[2:]

    # Adding leading zeros if necessary
    checksum_sum = checksum_sum.zfill(k)

    # Calculating the complement of sum
    checksum = ''.join('0' if bit == '1' else '1' for bit in checksum_sum)
    
    return checksum

def check_receiver_checksum(received_message, k, checksum):
    # Dividing received message into packets of k bits.
    c1 = received_message[:k]
    c2 = received_message[k:2*k]
    c3 = received_message[2*k:3*k]
    c4 = received_message[3*k:4*k]

    # Calculating the binary sum of packets + checksum
    receiver_sum = bin(int(c1, 2) + int(c2, 2) + int(checksum, 2) + int(c3, 2) + int(c4, 2) + int(checksum, 2))[2:]

    # Adding the overflow bits
    if len(receiver_sum) > k:
        x = len(receiver_sum) - k
        receiver_sum = bin(int(receiver_sum, 2) >> x)[2:]

    # Calculating the complement of sum
    receiver_checksum = ''.join('0' if bit == '1' else '1' for bit in receiver_sum)

    return receiver_checksum

# Example usage:
sent_message = "101110111000"
k = 4
checksum = find_checksum(sent_message, k)
print("Checksum:", checksum)

received_message = "101110111000"
receiver_checksum = check_receiver_checksum(received_message, k, checksum)
print("Receiver Checksum:", receiver_checksum)
